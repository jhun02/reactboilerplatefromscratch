const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
// console.log(path.join(__dirname, "public"));

module.exports = {
  entry: "./src/app.js",
  output: {
    path: path.join(__dirname, "public"),
    filename: "bundle.js"
  },

  module: {
    rules: [
      {
        //only the file that ends with js run babel-loader
        loader: "babel-loader",
        test: /\.js$/,
        exclude: /node_module/
      },
      {
        test: /\.s?css$/, // ? to support css file and scss file ? = optional
        //use provide us an array of loader
        use: ["style-loader", "css-loader", "sass-loader"]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: "./public/index.html"
    })
  ],
  devtool: "cheap-module-eval-source-map",
  devServer: {
    contentBase: path.join(__dirname, "public")
  }
};

// loader -> customize the behavior of webpack when it loads a given file
