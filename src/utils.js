console.log("from utils");

const squre = x => x * x;

const add = (a, b) => a + b;

const isAdult = age => age >= 18;

const canDrink = age => age >= 21;
export { squre, add, isAdult, canDrink };
